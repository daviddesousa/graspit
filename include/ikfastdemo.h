#ifndef _IKFASTDEMO_H_
#define _IKFASTDEMO_H_

int ikfastdemo(int argc, char** argv);
int ikcompute(double tx, double ty, double tz, double qw, double qx, double qy, double qz);

#endif