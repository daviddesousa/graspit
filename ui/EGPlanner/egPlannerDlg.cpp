//######################################################################
//
// GraspIt!
// Copyright (C) 2002-2009  Columbia University in the City of New York.
// All rights reserved.
//
// GraspIt! is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GraspIt! is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with GraspIt!.  If not, see <http://www.gnu.org/licenses/>.
//
// Author(s): Matei T. Ciocarlie
//
// $Id: egPlannerDlg.cpp,v 1.9 2009/07/02 21:04:05 cmatei Exp $
//
//######################################################################

#include "egPlannerDlg.h"

#include <QGridLayout>
#include <QCheckBox>
#include <QVBoxLayout>
#include <QCheckBox>
#include <QLabel>
#include <QHBoxLayout>
#include <QSlider>
#include <QLabel>
#include <QValidator>
#include <QFileDialog>

#include <Inventor/actions/SoWriteAction.h>

#include "search.h"
#include "searchState.h"
#include "body.h"
#include "robot.h"
#include "egPlanner.h"
#include "eigenGrasp.h"
#include "grasp.h"
#include "world.h"
#include "graspitCore.h"
#include "ivmgr.h"
#include "contactExaminerDlg.h"
#include "onLinePlanner.h"
#include "timeTest.h"
#include "guidedPlanner.h"
#include "loopPlanner.h"

#include <time.h>
#include <unistd.h>

//#define GRASPITDBG
#include "debug.h"

extern "C" {
    #include <qhull/qhull_a.h>
}

#include <QMutex>
#include <QThreadStorage>

#include <boost/geometry/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/multi/geometries/multi_point.hpp>
#include <boost/geometry/geometries/polygon.hpp>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/point_generators_3.h>
#include <CGAL/algorithm.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/convex_hull_3.h>
#include <vector>

#include <CGAL/Simple_cartesian.h>
#include <CGAL/AABB_tree.h>
#include <CGAL/AABB_traits.h>
#include <CGAL/Polyhedron_3.h>
//#include <CGAL/AABB_polyhedron_triangle_primitive.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/AABB_face_graph_triangle_primitive.h>

#include <CGAL/config.h>

#include <sys/time.h>
#include <tf/tf.h>

#include "ikfastdemo.h"

typedef CGAL::Exact_predicates_inexact_constructions_kernel  K;

typedef K::Point_3 Point;
typedef K::Plane_3 Plane;
typedef K::Vector_3 Vector;
typedef K::Segment_3 Segment;
typedef CGAL::Polyhedron_3<K> Polyhedron;
typedef CGAL::AABB_face_graph_triangle_primitive<Polyhedron> Primitive;
typedef CGAL::AABB_traits<K, Primitive> Traits;
typedef CGAL::AABB_tree<Traits> Tree;
typedef boost::optional< Tree::Intersection_and_primitive_id<Segment>::Type > Segment_intersection;
typedef boost::optional< Tree::Intersection_and_primitive_id<Plane>::Type > Plane_intersection;
typedef Tree::Primitive_id Primitive_id;

void EigenGraspPlannerDlg::exitButton_clicked()
{
 QDialog::accept();
}

void EigenGraspPlannerDlg::init()
{ 
 energyBox->insertItem("Hand Contacts");
 energyBox->insertItem("Potential Quality");
 energyBox->insertItem("Contacts AND Quality");
 energyBox->insertItem("Autograsp Quality");
 energyBox->insertItem("Guided Autograsp");
 energyBox->setCurrentItem(0);

 plannerTypeBox->insertItem("Sim. Ann.");
 //plannerTypeBox->insertItem("Loop");
 plannerTypeBox->insertItem("Multi-Threaded");
 //plannerTypeBox->insertItem("Online");
 //plannerTypeBox->insertItem("Time Test");
 plannerTypeBox->setCurrentItem(0);
 
 plannerInitButton->setEnabled(TRUE);
 plannerResetButton->setEnabled(FALSE);
 plannerStartButton->setEnabled(FALSE);
 instantEnergyButton->setEnabled(FALSE);

 useVirtualHandBox->setChecked(true);
 onlineDetailsGroup->setEnabled(FALSE);

 QString n;
 QIntValidator* vAnnSteps = new QIntValidator(1,500000,this);
 annStepsEdit->setValidator(vAnnSteps);
 n.setNum(70000);
 annStepsEdit->setText(n);

 spaceSearchBox->insertItem("Complete");
 spaceSearchBox->insertItem("Axis-angle");
 spaceSearchBox->insertItem("Ellipsoid");
 spaceSearchBox->insertItem("Approach");
 spaceSearchBox->setCurrentItem(1);

 prevGraspButton->setEnabled(FALSE);
 nextGraspButton->setEnabled(FALSE);
 bestGraspButton->setEnabled(FALSE);

 variableBox->setColumnLayout(0, Qt::Vertical);

 varGridLayout = new QGridLayout( variableBox->layout(),1,5 );
 varGridLayout->setSpacing(5);
 varGridLayout->setAlignment(Qt::AlignTop);
 varGridLayout->addMultiCellWidget(spaceSearchLabel,0,0,0,1);
 varGridLayout->addMultiCellWidget(spaceSearchBox,0,0,2,4);

 varGridLayout->addWidget( new QLabel("On", variableBox),1,0 );
 varGridLayout->addWidget( new QLabel("Name", variableBox),1,1 );
 varGridLayout->addWidget( new QLabel("Input", variableBox),1,2 );
 varGridLayout->addWidget( new QLabel("Target", variableBox),1,3 );
 varGridLayout->addWidget( new QLabel("Confidence", variableBox),1,4 );

 inputGloveBox->setEnabled(FALSE);
 inputLoadButton->setEnabled(FALSE);

 setState(INI);
 mIter = 0;

 fprintf(stderr,"INIT DONE \n");
}

void EigenGraspPlannerDlg::destroy()
{
 delete mHandObjectState;
 if (mPlanner) delete mPlanner;

 //cleanup
 for (unsigned int i=0; i<varNames.size(); i++) {
//  varMainLayout->removeItem(varLayouts[i]);

  varGridLayout->remove(varNames[i]);
  varGridLayout->remove(varCheck[i]);
  varGridLayout->remove(varInput[i]);
  varGridLayout->remove(varTarget[i]);
  varGridLayout->remove(varConfidence[i]);

  delete varNames[i];
  delete varCheck[i];
  delete varInput[i];
  delete varTarget[i];
  delete varConfidence[i];

 }
 varNames.clear();
 varCheck.clear();
 varInput.clear();
 varConfidence.clear();
 varTarget.clear();
 varLayouts.clear();
}

void EigenGraspPlannerDlg::setCurrentHand(int i)
{
  if(i < mHandList.size())
    mHand = mHandList[i];
  else
    DBGA("The Hand selected doesn't exist!");
}

void EigenGraspPlannerDlg::setMembers( std::vector<Hand*> h, GraspableBody *b )
{
 mPlanner = NULL;
 mHandList = h;
 setCurrentHand(0);
 mObject = b;
 mHand->getGrasp()->setObjectNoUpdate(mObject);
// mHand->getGrasp()->setGravity(true);
 mHand->getGrasp()->setGravity(false);

 mHandObjectState = new GraspPlanningState(mHand);
 mHandObjectState->setObject(mObject);
 mHandObjectState->setPositionType(SPACE_AXIS_ANGLE);
 mHandObjectState->setRefTran(mObject->getTran());
 mHandObjectState->reset();
 setVariableLayout();

 if (mHand->getNumVirtualContacts() > 0) {
	 setContactsBox->setChecked(TRUE);
 }

 updateVariableLayout();
 updateInputLayout();
}

// ----------------------------------- Search State and variable layout management -------------------------------

void EigenGraspPlannerDlg::setVariableLayout()
{
 //cleanup
 for (unsigned int i=0; i<varNames.size(); i++) {
  varGridLayout->remove(varNames[i]);
  varGridLayout->remove(varCheck[i]);
  varGridLayout->remove(varInput[i]);
  varGridLayout->remove(varTarget[i]);
  varGridLayout->remove(varConfidence[i]);
  delete varNames[i];
  delete varCheck[i];
  delete varInput[i];
  delete varTarget[i];
  delete varConfidence[i];
 }
 varNames.clear();
 varCheck.clear();
 varInput.clear();
 varTarget.clear();
 varConfidence.clear();
 varLayouts.clear();

 int size = mHand->getEigenGrasps()->getSize() + 7;

 for (int i=0; i<size; i++) {
  QLabel *name = new QLabel("foo",variableBox);
  QCheckBox *check = new QCheckBox(variableBox);
  connect(check, SIGNAL(clicked()), this, SLOT(variableCheckBoxChanged()));
  QCheckBox *inputCheck = new QCheckBox(variableBox);
  connect(inputCheck, SIGNAL(clicked()), this, SLOT(variableInputChanged()));
  QLabel *target = new QLabel("N/A",variableBox);
  QSlider *slider = new QSlider(0,100,10,0,Qt::Horizontal,variableBox);
  connect(slider, SIGNAL(sliderReleased()), this, SLOT(variableInputChanged()));

  slider->setLineStep(1);
  slider->setMaximumWidth(50);
  varGridLayout->addWidget(check,2+i,0);
  varGridLayout->addWidget(name,2+i,1);
  varGridLayout->addWidget(inputCheck,2+i,2);
  varGridLayout->addWidget(target,2+i,3);
  varGridLayout->addWidget(slider,2+i,4);
  

  varCheck.push_back( check );
  varNames.push_back( name );
  varInput.push_back( inputCheck );
  varTarget.push_back( target );
  varConfidence.push_back( slider );
 }
}

void EigenGraspPlannerDlg::updateVariableLayout()
{
	int i;
 for (i=0; i<mHandObjectState->getNumVariables(); i++) {
  varNames[i]->setEnabled(TRUE);
  varNames[i]->setText( mHandObjectState->getVariable(i)->getName() );
  varCheck[i]->setEnabled(TRUE);
  if (mHandObjectState->getVariable(i)->isFixed()) varCheck[i]->setChecked(false);
  else varCheck[i]->setChecked(true);
 }

 for (i=mHandObjectState->getNumVariables(); i < mHand->getEigenGrasps()->getSize() + 7; i++) {
  varNames[i]->setEnabled(FALSE);
  varNames[i]->setText( "n/a" );
  varCheck[i]->setChecked(false);
  varCheck[i]->setEnabled(FALSE);
 }
}

void EigenGraspPlannerDlg::updateInputLayout()
{
	int i;
 for (i=0; i<mHandObjectState->getNumVariables(); i++) {
   if ( !mPlanner || !(mPlanner->isReady() || mPlanner->isActive()) ) {
   varInput[i]->setEnabled(FALSE);
   varInput[i]->setChecked(false);
   varTarget[i]->setText("N/A");
   varTarget[i]->setEnabled(FALSE);
   varConfidence[i]->setValue( 0 );
   varConfidence[i]->setEnabled(FALSE);
  } else {
   GraspPlanningState *t = mPlanner->getTargetState();
   varInput[i]->setEnabled(TRUE);
   QString n;
   n.setNum(t->getVariable(i)->getValue(),'f',3);
   varTarget[i]->setText(n);
   varConfidence[i]->setValue( t->getVariable(i)->getConfidence() * 100 );
   if ( t->getVariable(i)->isFixed() ) {
    varInput[i]->setChecked(TRUE);
    varTarget[i]->setEnabled(TRUE);
    varConfidence[i]->setEnabled(TRUE);
   } else {
    varInput[i]->setChecked(FALSE);
    varTarget[i]->setEnabled(FALSE);
    varConfidence[i]->setEnabled(FALSE);
   }
  }
 }

 for (i=mHandObjectState->getNumVariables(); i < mHand->getEigenGrasps()->getSize() + 7; i++) {
  varInput[i]->setEnabled(FALSE);
  varInput[i]->setChecked(false);
  varTarget[i]->setText("N/A");
  varTarget[i]->setEnabled(FALSE);
  varConfidence[i]->setValue( 0 );
  varConfidence[i]->setEnabled(FALSE);
 }
}

void EigenGraspPlannerDlg::variableInputChanged()
{
	assert(mPlanner);
	GraspPlanningState *t = mPlanner->getTargetState();
	assert(t);
	for (int i=0; i<mHandObjectState->getNumVariables(); i++) {
		if (varInput[i]->isChecked()) {
			varTarget[i]->setEnabled(TRUE);
			varConfidence[i]->setEnabled(TRUE);
			t->getVariable(i)->setFixed(true);
			t->getVariable(i)->setConfidence( ((double)varConfidence[i]->value()) / 100.0);
			DBGP("Conf: " << ((double)varConfidence[i]->value()) / 100.0);
		}
		else {
			varTarget[i]->setEnabled(FALSE);
			t->getVariable(i)->setFixed(false);
			t->getVariable(i)->setConfidence(0);
			varConfidence[i]->setValue(0);
			varConfidence[i]->setEnabled(FALSE);
		}
	}
}

void EigenGraspPlannerDlg::variableCheckBoxChanged()
{
 for (int i=0; i<mHandObjectState->getNumVariables(); i++) {
  if (varCheck[i]->isChecked()) mHandObjectState->getVariable(i)->setFixed(false);
  else mHandObjectState->getVariable(i)->setFixed(true);
 }
 //force a reset of the planner
 plannerStartButton->setEnabled(FALSE);
}

void EigenGraspPlannerDlg::spaceSearchBox_activated( const QString &s )
{
 if ( s==QString("Complete") ) {
  mHandObjectState->setPositionType(SPACE_COMPLETE);
  mHandObjectState->setRefTran( mObject->getTran() );
 }  else if ( s==QString("Axis-angle") ) {
  mHandObjectState->setPositionType(SPACE_AXIS_ANGLE);
  mHandObjectState->setRefTran( mObject->getTran() );
 } else if ( s==QString("Ellipsoid") ) {
  mHandObjectState->setPositionType(SPACE_ELLIPSOID);
  mHandObjectState->setRefTran( mObject->getTran() );
 } else if ( s==QString("Approach") ) {
  mHandObjectState->setPositionType(SPACE_APPROACH);
  mHandObjectState->setRefTran( mHand->getTran() );
 } else {
  fprintf(stderr,"WRONG SEARCH TYPE IN DROP BOX!\n");
 }
 mHandObjectState->reset();
 updateVariableLayout();
 //force a reset of the planner
 if (mPlanner) mPlanner->invalidateReset();
 updateStatus();
}

//------------------------------------- Show Results stuff ---------------------------------

void EigenGraspPlannerDlg::prevGraspButton_clicked()
{
 mDisplayState--;
 updateResults(true);
}

void EigenGraspPlannerDlg::bestGraspButton_clicked()
{
 if (!mPlanner) return;
 mDisplayState = 0;
 updateResults(true);
}

void EigenGraspPlannerDlg::nextGraspButton_clicked()
{
 mDisplayState++;
 updateResults(true);
}

void EigenGraspPlannerDlg::plannerUpdate()
{
 assert(mPlanner);
 mDisplayState = 0;
 updateResults(false);
 //if we are using the CyberGlove for input this updates the target values
 if (inputGloveBox->isChecked()) {
	 updateInputLayout();
 }
}

void EigenGraspPlannerDlg::updateResults(bool render)
{
  DBGA("updateR: Updating results");

 assert(mPlanner);

 QString nStr;
 nStr.setNum(mPlanner->getCurrentStep());
 currentStepLabel->setText(QString("Current step: ") + nStr);

 nStr.setNum(mPlanner->getRunningTime(),'f',3);
 timeLabel->setText(QString("Time used: ") + nStr + QString(" sec"));

 int d = mPlanner->getListSize();
 int rank, size, iteration; double energy;

 if (d==0) {
  mDisplayState = rank = size = energy = iteration = 0; render = false;
 } else if (mDisplayState < 0){
  mDisplayState = 0;
 } else if ( mDisplayState >= d) {
  mDisplayState = d-1;
 } 
 
 if ( d!=0 ){
  const GraspPlanningState *s = mPlanner->getGrasp( mDisplayState);
  rank = mDisplayState+1;
  size = d;
  iteration = s->getItNumber();
  energy = s->getEnergy();
 }

 QString n1,n2;
 n1.setNum(rank);
 n2.setNum(size);
 rankLabel->setText("Rank: " + n1 + "/" + n2);
 n1.setNum(iteration);
 iterationLabel->setText("Iteration: " + n1);
 n1.setNum(energy,'f',3);
 energyLabel->setText("Energy: " + n1);

 DBGA("updateR: Rendering and render: " << render);

 if (render) {

  std::list<GraspPlanningState*> mBestListTemp;

  if(mState == FIN) {
    DBGA("\nupdateR: Current state is FIN");
  }
  else {
    DBGA("\nupdateR: Current state: " << mState);
  }

  /// If the whole algo has finished
  if(mState == FIN) {

    if(mDisplayState >= mBestListHand1.size()) mDisplayState = mBestListHand1.size() - 1;

    // Select second hand
    setCurrentHand(1);

    for(int i=0; i < mAllBestListHand2.size(); i++)
      DBGA("updateR: mAllBestListHand2[" << i << "] size: " << mAllBestListHand2[i].size());

    DBGA("updateR: Showing the second hand pose for this iteration... mDisplayState: " << mDisplayState);

    // according to mDisplayState select the respective second hand pose
    std::list<GraspPlanningState*>::iterator it = mAllBestListHand2[mDisplayState].begin();
    mPlanner->showGrasp(*it);

    DBGA("updateR: Continuing...");

    // Select first hand
    setCurrentHand(0);
    mBestListTemp = mBestListHand1;
    //DBGA("Current Hand is 0, list size is " << mBestListTemp.size());

    for(int i=0; i < graspQuality[mDisplayState].size(); i++) {
      DBGA("updateR: Quality of grasp " << i << ": " << graspQuality[mDisplayState][i]);
    }

  } else {

    if(mHand == mHandList[0] && mBestListHand1.size() > 0) {
      mBestListTemp = mBestListHand1;
      DBGA("updateR: Current Hand is 0, list size is " << mBestListTemp.size());
    }
    else if(mHand == mHandList[1]) {
      mBestListTemp = mBestListHand2;
      DBGA("updateR: Current Hand is 1, list size is " << mBestListTemp.size());
    }
    else {
      mBestListTemp = mPlanner->mBestList;
      DBGA("Hand not found! Have you seen it?");
    }

    if(mDisplayState<0 || mDisplayState>mBestListTemp.size()) {
      DBGA("updateR: Grasp does not exist");
      return;
    }
  }

  DBGA("updateR: mDisplayState:" << mDisplayState);

  std::list<GraspPlanningState*>::iterator it = mBestListTemp.begin();
  std::advance(it, mDisplayState);

  mPlanner->showGrasp(*it);

  //mHand->getWorld()->findAllContacts();
  //mHand->getGrasp()->update();
  mPlanner->getGrasp(mDisplayState)->printState();
  //mHand->autoGrasp(true);
 }
}

// ----------------------------- Settings management ---------------------------


void EigenGraspPlannerDlg::updateStatus()
{
	PlannerState s = DONE;
	if (mPlanner) s = mPlanner->getState();
	DBGP("Update Layout");
	switch(s) {
		case INIT:
			plannerInitButton->setEnabled(FALSE);
			plannerResetButton->setEnabled(TRUE);
			plannerStartButton->setEnabled(FALSE);
			plannerStartButton->setText(">");

			prevGraspButton->setEnabled(FALSE);
			nextGraspButton->setEnabled(FALSE);
			bestGraspButton->setEnabled(FALSE);
			mObject->showFrictionCones(true);

			inputGloveBox->setEnabled(FALSE);
			inputLoadButton->setEnabled(FALSE);
			onlineDetailsGroup->setEnabled(FALSE);
			break;
		case READY:
			{
			plannerInitButton->setEnabled(FALSE);
			plannerResetButton->setEnabled(TRUE);
			plannerStartButton->setEnabled(TRUE);
			plannerStartButton->setText(">");

			prevGraspButton->setEnabled(TRUE);
			nextGraspButton->setEnabled(TRUE);
			bestGraspButton->setEnabled(TRUE);
			mObject->showFrictionCones(true);

			inputGloveBox->setEnabled(TRUE);
			inputLoadButton->setEnabled(TRUE);
			if (mPlanner->getType() == PLANNER_ONLINE) onlineDetailsGroup->setEnabled(TRUE);
			else onlineDetailsGroup->setEnabled(FALSE);
			}
			break;
		case RUNNING:
			plannerInitButton->setEnabled(FALSE);
			plannerResetButton->setEnabled(FALSE);
			plannerStartButton->setEnabled(TRUE);
			plannerStartButton->setText("||");

			prevGraspButton->setEnabled(FALSE);
			nextGraspButton->setEnabled(FALSE);
			bestGraspButton->setEnabled(FALSE);
			mObject->showFrictionCones(false);
			break;
		default:
			plannerInitButton->setEnabled(TRUE);
			plannerResetButton->setEnabled(FALSE);
			plannerStartButton->setEnabled(FALSE);
			plannerStartButton->setText(">");

			prevGraspButton->setEnabled(FALSE);
			nextGraspButton->setEnabled(FALSE);
			bestGraspButton->setEnabled(FALSE);
			mObject->showFrictionCones(true);

			inputGloveBox->setEnabled(FALSE);
			inputLoadButton->setEnabled(FALSE);
			onlineDetailsGroup->setEnabled(FALSE);
			break;
	}
	updateInputLayout();
}

void EigenGraspPlannerDlg::energyBox_activated( const QString & )
{
 //force a reset of the planner
	if (mPlanner) mPlanner->invalidateReset();
	updateStatus();
}

void EigenGraspPlannerDlg::setContactsBox_toggled( bool checked)
{
 if (checked) {
  if ( mHand->getNumVirtualContacts() == 0 ) {
   //if we are asking to use pre-set contacts, but none are available, pop up the dialog
   //for loading virtual contacts
   ContactExaminerDlg dlg(this);
   dlg.exec();
  }
  if (mHand->getNumVirtualContacts() == 0) {
   //if we still have no virtual contacts, un-check the box
   setContactsBox->setChecked(false);
  }
 }
 if (mPlanner) mPlanner->invalidateReset();
 updateStatus();
}

void EigenGraspPlannerDlg::readPlannerSettings()
{
 assert(mPlanner);
 //energy type
 QString s = energyBox->currentText();
 if ( s == QString("Hand Contacts") ) {
  mPlanner->setEnergyType(ENERGY_CONTACT);
 } else if ( s == QString("Potential Quality") ) {
  mPlanner->setEnergyType(ENERGY_POTENTIAL_QUALITY);
 } else if ( s == QString("Autograsp Quality") ) {
  mPlanner->setEnergyType(ENERGY_AUTOGRASP_QUALITY);
 } else if ( s == QString("Contacts AND Quality") ) {
  mPlanner->setEnergyType(ENERGY_CONTACT_QUALITY);
 } else if ( s == QString("Guided Autograsp") ) {
  mPlanner->setEnergyType(ENERGY_GUIDED_AUTOGRASP);
 } else {
  fprintf(stderr,"WRONG ENERGY TYPE IN DROP BOX!\n");
 }

 //contact type
 if ( setContactsBox->isChecked() ) {
  mPlanner->setContactType(CONTACT_PRESET);
 } else {
  mPlanner->setContactType(CONTACT_LIVE);
 }

 //steps
 int steps = annStepsEdit->text().toInt();
 mPlanner->setMaxSteps(steps);
}

long getMicrotime(){
  struct timeval currentTime;
  gettimeofday(&currentTime, NULL);
  return currentTime.tv_sec * (int)1e6 + currentTime.tv_usec;
}

/**
  Returns the number of collisions of the convex hulls of both grippers
  The convex hull is "downsampled" to 1/3 of the points due to performance issues
*/
int
EigenGraspPlannerDlg::getCollisions()
{
  long start = getMicrotime();

  std::vector<DynamicBody *> allLinkVec1, allLinkVec2;

  /// mHand get Vertices and add them
  mHandList[0]->getAllLinks(allLinkVec1);

  std::vector<position> vertices_mHand;

  for(int i=0; i < allLinkVec1.size(); i++) {
//            DBGA("Link(" << i << "): " << allLinkVec1[i]->getTran());

      std::vector<position> vertices_tmp;
      allLinkVec1[i]->getGeometryVertices(&vertices_tmp);

      for(int j=0; j < vertices_tmp.size(); j++) {
          vertices_tmp[j] = vertices_tmp[j] * allLinkVec1[i]->getTran();
      }

      vertices_mHand.insert(vertices_mHand.end(), vertices_tmp.begin(), vertices_tmp.end());
  }

  /// mOtherHand get Vertices and add them
  mHandList[1]->getAllLinks(allLinkVec2);

  std::vector<position> vertices_mOtherHand;

  for(int i=0; i < allLinkVec2.size(); i++) {
//            DBGA("Link(" << i << "): " << allLinkVec2[i]->getTran());

      std::vector<position> vertices_tmp;
      allLinkVec2[i]->getGeometryVertices(&vertices_tmp);

      for(int j=0; j < vertices_tmp.size(); j++) {
          vertices_tmp[j] = vertices_tmp[j] * allLinkVec2[i]->getTran();
      }

      vertices_mOtherHand.insert(vertices_mOtherHand.end(), vertices_tmp.begin(), vertices_tmp.end());
  }

  /// Copying the vertices to a Point vector to construct the polyhedrons
  std::vector<Point> points1, points2;
  Polyhedron poly1;
  Polyhedron poly2;

//        DBGA("\n\nAdding points. Total mHand: " << vertices_mHand.size() << " Total mOtherHand: " << vertices_mOtherHand.size() );

  for(int i=0; i < vertices_mHand.size(); i = i + 1) {

      //DBGA("Point(" << i << "): " << vertices_mHand[i]);
      points1.push_back(Point(vertices_mHand[i].x(), vertices_mHand[i].y(), vertices_mHand[i].z()));
  }

  for(int i=0; i < vertices_mOtherHand.size(); i = i + 1) {

      points2.push_back(Point(vertices_mOtherHand[i].x(), vertices_mOtherHand[i].y(), vertices_mOtherHand[i].z()));
  }

//        DBGA("Finished. Computing convex hull now...");

  /// Compute convex hull of non-collinear points
  CGAL::convex_hull_3(points1.begin(), points1.end(), poly1);
//        DBGA("The convex hull 1 contains " << poly1.size_of_vertices() << " vertices and " << poly1.size_of_facets() << " facets.");

  CGAL::convex_hull_3(points2.begin(), points2.end(), poly2);
//        DBGA("The convex hull 2 contains " << poly2.size_of_vertices() << " vertices and " << poly2.size_of_facets() << " facets.");

  // constructs AABB tree 
  Tree tree(faces(poly1).first, faces(poly1).second, poly1);

  int edges=0, collisions=0;

  for(Polyhedron::Edge_iterator edge_it = poly2.edges_begin(); edge_it != poly2.edges_end(); edge_it++) {

    Segment segment_temp(edge_it->vertex()->point(), edge_it->opposite()->vertex()->point());

    //DBGA("Point1: " << edge_it->vertex()->point() << "\t\tPoint2: " << edge_it->opposite()->vertex()->point());

    if(tree.do_intersect(segment_temp))
      collisions++;
  
    edges++;
  }

  //DBGA("It were tested " << edges << " edges and " << collisions << " collisions were found!");

  long end = getMicrotime();
  long ttime = end - start;

  DBGA("\n\nConvex hull time: " << ttime << "   start time: " << start << "  end time: " << end);

  return collisions;
}


// Object position in MoveIt!
extern tf::Quaternion moveit_object_q;
extern tf::Vector3    moveit_object_t;
extern tf::Transform  moveit_object;      

// Transform from the GraspIt! gripper frame to the MoveIt! gripper frame
//tf::Quaternion graspit_gripper_to_object_q(0,0,0,0); //= EulerZYX_to_Quaternion(0.0, -M_PI/2, 0.0);
//tf::Vector3    graspit_gripper_to_object_t(0.0, 0.0, 0.0);
//tf::Transform  graspit_gripper_to_object(graspit_gripper_to_object_q, graspit_gripper_to_object_t);


// Transform from the GraspIt! gripper frame to the MoveIt! gripper frame
extern tf::Quaternion moveit_to_graspit_gripper_q; //= EulerZYX_to_Quaternion(0.0, -M_PI/2, 0.0);
extern tf::Vector3    moveit_to_graspit_gripper_t;
extern tf::Transform  moveit_to_graspit_gripper;

/**
  Evaluates the IK-feseability of the grasps
*/
int 
EigenGraspPlannerDlg::evalIK(std::list<GraspPlanningState*> grasp_list)
{
  int num_valid = 0;

  // Create a temporary sorted list
  for(int j=0; j < grasp_list.size(); j++) {
    std::list<GraspPlanningState*>::iterator it = grasp_list.begin();

    std::advance(it, j);

    //! Create a new local variable to store the grasp
    GraspPlanningState *mTempState = new GraspPlanningState(*it);

    /***/

    PositionState *mPosition;
    mPosition = PositionState::createInstance(SPACE_COMPLETE, mTempState->getHand());
    mPosition =  mTempState->getPosition();

    transf HandPos = mPosition->getCoreTran();

    vec3    HandTranlation = HandPos.translation();
    Quaternion  HandRotation   = HandPos.rotation();

    DBGA("Tx: " << HandTranlation.x() << " Ty: " << HandTranlation.y() << " Tz: " << HandTranlation.z());
    DBGA("Qw: " << HandRotation.w << " Qx: " << HandRotation.x << " Qy: " << HandRotation.y << " Qz: " << HandRotation.z);

    tf::Quaternion graspit_gripper_to_object_q(HandRotation.x, HandRotation.y, HandRotation.z, HandRotation.w);
    tf::Vector3    graspit_gripper_to_object_t(HandTranlation.x() * 0.001, HandTranlation.y() * 0.001, HandTranlation.z() * 0.001);
    tf::Transform  graspit_gripper_to_object(graspit_gripper_to_object_q, graspit_gripper_to_object_t);

    tf::Transform  graspit_gripper = moveit_object * graspit_gripper_to_object * moveit_to_graspit_gripper;

    tf::Vector3    gripper_t = graspit_gripper.getOrigin();
    tf::Quaternion gripper_q = graspit_gripper.getRotation();

    DBGA("In MoveIt! frame:");
    DBGA("Tx: " << gripper_t.getX() << " Ty: " << gripper_t.getY() << " Tz: " << gripper_t.getZ());
    DBGA("Qw: " << gripper_q.w() << " Qx: " << gripper_q.x() << " Qy: " << gripper_q.y() << " Qz: " << gripper_q.z());

    int res = ikcompute(  gripper_t.getX(),   // tx
                          gripper_t.getY(),   // ty
                          gripper_t.getZ(),   // tz
                          gripper_q.w(),      // qw
                          gripper_q.x(),      // qx
                          gripper_q.y(),      // qy
                          gripper_q.z()  );   // qz

    if(res > 0) {
      DBGA("This one is IK valid...");
      num_valid++;
    } else {
      DBGA("This one is NOT IK valid!");
    }
  }

  DBGA("\nThere are " << num_valid << " IK-feasible grasp out of " << grasp_list.size());

  return num_valid;
}

/**
  Sorts the grasps according to their volume and epsilon quality
  obtained from the autoGrasp energy calculator
*/
void
EigenGraspPlannerDlg::evalGrasps()
{
  // set energy autograspQualityEnergy 
  mPlanner->setEnergyType(ENERGY_AUTOGRASP_QUALITY);

  double qual_vals[20][2];              //! auxiliar array to store the sort grasps
  std::vector<double> tmp_grasp_qual;   //! auxiliar vector to insert the sort grasps on the graspQuality vector

  DBGA("evalGrasps: Beginning evaluation");

  // iterate over mBestList
  for(int i=0; i < mPlanner->mBestList.size(); i++) {

    double val = mPlanner->showGrasp(i);

    DBGA("evalGrasps: Evaluate result: " << val);

    int collisions = getCollisions();    

    //! If there are collisions between the CH penalize the quality value
    if(collisions > 0)
      val += 100;

    //! Save the current grasp in the aux array
    qual_vals[i][0] = i;
    qual_vals[i][1] = val;
  }

  double temp[2];

  //! Sort the grasps by quality
  for(int j=0; j < mPlanner->mBestList.size(); j++) {
    for(int i=0; i < mPlanner->mBestList.size() - 1; i++) {
      if(qual_vals[i][1] > qual_vals[i+1][1]) {
        temp[0] = qual_vals[i][0]; temp[1] = qual_vals[i][1];
        qual_vals[i][0] = qual_vals[i+1][0]; qual_vals[i][1] = qual_vals[i+1][1];
        qual_vals[i+1][0] = temp[0]; qual_vals[i+1][1] = temp[1];
      }
    }
  }

  std::list<GraspPlanningState*> mBestTempList;

  int num_valid = 0;

  // Create a temporary sorted list
  for(int j=0; j < mPlanner->mBestList.size(); j++) {
    std::list<GraspPlanningState*>::iterator it = mPlanner->mBestList.begin();

    int idx = (int) qual_vals[j][0];
    std::advance(it, idx);

    tmp_grasp_qual.push_back(qual_vals[idx][0]);

    //! Create a new local variable to store the grasp
    GraspPlanningState *mTempState = new GraspPlanningState(*it);

    /***

    PositionState *mPosition;
    mPosition = PositionState::createInstance(SPACE_COMPLETE, mTempState->getHand());
    mPosition =  mTempState->getPosition();

    transf HandPos = mPosition->getCoreTran();

    vec3    HandTranlation = HandPos.translation();
    Quaternion  HandRotation   = HandPos.rotation();

    DBGA("Tx: " << HandTranlation.x() << " Ty: " << HandTranlation.y() << " Tz: " << HandTranlation.z());
    DBGA("Qw: " << HandRotation.w << " Qx: " << HandRotation.x << " Qy: " << HandRotation.y << " Qz: " << HandRotation.z);

    tf::Quaternion graspit_gripper_to_object_q(HandRotation.x, HandRotation.y, HandRotation.z, HandRotation.w);
    tf::Vector3    graspit_gripper_to_object_t(HandTranlation.x() * 0.001, HandTranlation.y() * 0.001, HandTranlation.z() * 0.001);
    tf::Transform  graspit_gripper_to_object(graspit_gripper_to_object_q, graspit_gripper_to_object_t);

    tf::Transform  graspit_gripper = moveit_object * graspit_gripper_to_object * moveit_to_graspit_gripper;

    tf::Vector3    gripper_t = graspit_gripper.getOrigin();
    tf::Quaternion gripper_q = graspit_gripper.getRotation();

    DBGA("In MoveIt! frame:");
    DBGA("Tx: " << gripper_t.getX() << " Ty: " << gripper_t.getY() << " Tz: " << gripper_t.getZ());
    DBGA("Qw: " << gripper_q.w() << " Qx: " << gripper_q.x() << " Qy: " << gripper_q.y() << " Qz: " << gripper_q.z());

    int res = ikcompute(  gripper_t.getX(),   // tx
                          gripper_t.getY(),   // ty
                          gripper_t.getZ(),   // tz
                          gripper_q.w(),      // qw
                          gripper_q.x(),      // qx
                          gripper_q.y(),      // qy
                          gripper_q.z()  );   // qz

    if(res > 0) {
      DBGA("This one is IK valid...");
      num_valid++;
    } else {
      DBGA("This one is NOT IK valid!");
    }

    ***/

    mBestTempList.push_back(mTempState);
  }

  //! If this evalution corresponds to one of the second gripper add it to the graspQuality vector
  if(mHand == mHandList[1])
    graspQuality.push_back(tmp_grasp_qual);

  if(mHandList[0] == mHand) {
    mBestListHand1 = mBestTempList;
  } else if(mHandList[1] == mHand) {
    mBestListHand2 = mBestTempList;
    mAllBestListHand2.push_back(mBestListHand2);
  } else {
    DBGA("Something isn't right. The hand requested doesn't exist...");
    exit(-1);
  } 
}

int EigenGraspPlannerDlg::saveGrasps()
{
  DBGA("Saving the Grasps!");

  char fileName[256];
  //struct tm y2k = {0};

  FILE * pFile;

  sprintf(fileName, "~/grasps_results/grasps.txt");

  pFile = fopen(fileName, "w");

  if(pFile == NULL) DBGA("Can't create file!");

  std::string filename = mObject->getFilename().latin1();

  std::string slash = "/";
  std::string dot = ".";
  objectName = filename.substr(filename.rfind(slash)+1, filename.rfind(dot) - filename.rfind(slash) - 1);

  fprintf(pFile, "%s\n", objectName.c_str());

  for(int i=0; i < mBestListHand1.size(); i++) {
    
    std::list<GraspPlanningState*>::iterator it = mBestListHand1.begin();
    std::advance(it, i);

    DBGA("Saving the grasp " << i);

    (*it)->setPositionType(SPACE_COMPLETE, true);
    (*it)->saveToFile(pFile);
  }

  fclose(pFile);
}

void EigenGraspPlannerDlg::plannerComplete()
{
  DBGA("pComplete: plannerCompleted!");



  if(mState == RUN && mHand == mHandList[0]) {
    
    DBGA("pComplete: Finished Hand 1. Evaluating...");

    setState(EVAL);
  
    evalGrasps();

    time(&end_timer);
    if(total_time > 0.0) total_time = 0;

    total_time += difftime(end_timer, start_timer);

    evalIK(mBestListHand1);

    saveGrasps();

    DBGA("pComplete: Evaluation completed...");

    setState(WAIT);

    sleep(1);

    //startPlanner();

  } else if(mState == RUN && mHand == mHandList[1]) {

    setState(EVAL);
  
    evalGrasps();

    time(&end_timer);
    total_time += difftime(end_timer, start_timer);

    DBGA("pComplete: Finished Hand 2. Evaluating...");

    HandQual Hand1Qual[graspQuality.size()];

    // If this was the last iteraction
    if(mIter >= mBestListHand1.size()) {

      DBGA("\n\nExecution time: " << total_time << "\n\n");
     
      DBGA("\n\npComplete: Finished Iterations!");

      DBGA("pComplete: graspQuality size: " << graspQuality.size());
      
      for(int i=0; i<graspQuality.size(); i++) {
        DBGA("pComplete: graspQuality[" << i << "] size: " << graspQuality[i].size());
      }

      // Filling an array of tuples (index, quality)
      for(int i=0; i < graspQuality.size(); i++) {
        double totalQual = 0;

        DBGA("pComplete: i: " << i);

        for(int j=0; j < graspQuality[i].size(); j++) {

          DBGA("pComplete: j: " << j);

          totalQual += graspQuality[i][j];
        }

        Hand1Qual[i].index = i;
        Hand1Qual[i].qual  = (totalQual / graspQuality[i].size());
      }

      DBGA("pComplete: Sorting...");

      // sort by quality
      for(int j=0; j < graspQuality.size(); j++) {
        for(int i=0; i < graspQuality.size() - 1; i++) {
          if(Hand1Qual[i].qual > Hand1Qual[i+1].qual) {
            HandQual temp = Hand1Qual[i];

            Hand1Qual[i]   = Hand1Qual[i+1];
            Hand1Qual[i+1] = temp;
          }
        }
      }

      std::list<GraspPlanningState*> mBestTempList;

      DBGA("pComplete: Creating temporary list...");

      // create a temporary sorted list
      for(int j=0; j < mPlanner->mBestList.size(); j++) {
        std::list<GraspPlanningState*>::iterator it = mBestListHand1.begin();

        int idx = (int) Hand1Qual[j].index;

        DBGA("pComplete: Index: " << idx);

        std::advance(it, idx);

        GraspPlanningState *mTempState = new GraspPlanningState(*it);

        mBestTempList.push_back(mTempState);
      }

      DBGA("pComplete: Temporary list created");

      mBestListHand1 = mBestTempList;

      DBGA("pComplete: mAllBestListHand2 size: " << mAllBestListHand2.size());

      for(int i=0; i < mAllBestListHand2.size(); i++)
        DBGA("pComplete: mAllBestListHand2[" << i << "] size: " << mAllBestListHand2[i].size());

      // Sort the mAllBestListHand2 according to new order of the mBestListHand1
      std::vector< std::list<GraspPlanningState*> > mAllTempList;
      for(int j=0; j < mAllBestListHand2.size(); j++) {
        std::list<GraspPlanningState*>::iterator it = mBestListHand1.begin();

        mAllTempList.push_back(mAllBestListHand2[Hand1Qual[j].index]);
      }      

      mAllBestListHand2 = mAllTempList;

      DBGA("pComplete: Ready to show results!");

      setCurrentHand(0);

      setState(FIN);

      // Save the best grasps to a .iv file
      for(int i=0; i < mBestListHand1.size(); i++) {
        mDisplayState = i;
        updateResults(true);

        SoOutput out;

        std::string filename = "/home/david/catkin_ws/grasps_results/" + objectName + std::to_string(i) + ".iv";

        if(!out.openFile(filename.c_str()))
          DBGA("Error saving the .iv file");

        out.setBinary(false);
        SoWriteAction write(&out);

        write.apply( graspitCore->getIVmgr()->world->getIVRoot() );

        write.getOutput()->closeFile();
      }

      saveGrasps();

    } else {
     
      // If we're not finished yet
      setState(WAIT);

      sleep(1);

      //startPlanner();
    }

    DBGA("pComplete: State changed! Current iteration: " << mIter);

    //startPlanner();
  }

/*
  // Add the the Grasp from mBestList to either local list
  for(std::list<GraspPlanningState*>::iterator it = mPlanner->mBestList.begin(); it != mPlanner->mBestList.end(); it++) {
    GraspPlanningState *tmpGPS = new GraspPlanningState(*it);

    if(mHand == mHandList[0]) {
      mBestListHand1.push_back(tmpGPS);
    } else if(mHand == mHandList[1]) { 
      mBestListHand2.push_back(tmpGPS);
    }
  }

  // If there's more than one hand and we just finnish testing the first one

  /// TODO: if the simulation is paused before finishing don't change hands
  
  if(mHandList.size() > 1 && mHand == mHandList[0]) {
    setCurrentHand(1);
    plannerInit_clicked();
  }
*/

  updateStatus();
  bestGraspButton_clicked();
}
//----------------------------------- Planner start / stop control stuff ---------------------------

void EigenGraspPlannerDlg::plannerInit_clicked()
{
  QString s = plannerTypeBox->currentText();
  if(s == QString("Sim. Ann.")) {
    
    if(mPlanner) {
      delete mPlanner;
      DBGA("Planner deleted...");
    }
    
    
    
    if(mHandList.size() > 1 && mHand == mHandList[1]) {
      mPlanner = new SimAnnPlanner(mHand, false);
      ((SimAnnPlanner*)mPlanner)->setModelState(mHandObjectState);
      mPlanner->setSecondHand(mHandList[0]);
    } else {
      mPlanner = new SimAnnPlanner(mHand, true);
      ((SimAnnPlanner*)mPlanner)->setModelState(mHandObjectState);
    }

    energyBox->setEnabled(TRUE);
  } else if(s == QString("Loop")) {
  
    if(mPlanner) delete mPlanner;
    mPlanner = new LoopPlanner(mHand);
  
    ((LoopPlanner*)mPlanner)->setModelState(mHandObjectState);
    energyBox->setEnabled(TRUE);
  } else if(s == QString("Multi-Threaded")) {
    
    if(mPlanner) delete mPlanner;
    mPlanner = new GuidedPlanner(mHand);
    
    ((GuidedPlanner*)mPlanner)->setModelState(mHandObjectState);
    energyBox->setCurrentItem(2);
    energyBox->setEnabled(FALSE);
  } else if(s == QString("Online") ) {
    
    if(mPlanner) delete mPlanner;
    mPlanner = new OnLinePlanner(mHand);
    
    ((OnLinePlanner*)mPlanner)->setModelState(mHandObjectState);
    energyBox->setEnabled(TRUE);
    energyBox->setCurrentItem(2);
    
    QString n;
    n.setNum(2000);
    annStepsEdit->setText(n);
    QObject::connect(mPlanner,SIGNAL(update()),this,SLOT(onlinePlannerUpdate())); 
  } else if( s == QString("Time Test") ) {
    
    if(mPlanner) delete mPlanner;
    mPlanner = new MTTester(mHand);
  } else {
    
    fprintf(stderr,"Unknown planner type requested\n");
    return;
  }

  QObject::connect(mPlanner,SIGNAL(update()),this,SLOT(plannerUpdate()));
  QObject::connect(mPlanner,SIGNAL(complete()),this,SLOT(plannerComplete()));

  updateStatus();

  plannerReset_clicked();

  DBGA("\ninitClicked: Hands addrs:\n[0]: " << mHandList[0] << "\n[1]: " << mHandList[1]);

  std::string filename = mObject->getFilename().latin1();

  DBGA("initClicked: File name: " << filename);

  std::string slash = "/";
  std::string dot = ".";
  objectName = filename.substr(filename.rfind(slash)+1, filename.rfind(dot) - filename.rfind(slash) - 1); // token is "scott"

  DBGA("initClicked: Object name: " << objectName);

  //system("ivview /home/david/catkin_ws/output.iv");
}

void EigenGraspPlannerDlg::plannerReset_clicked() 
{
	assert(mPlanner);
	readPlannerSettings();
  mPlanner->resetPlanner();
  updateStatus();
}

void EigenGraspPlannerDlg::startPlanner()
{
  DBGA("startPlanner: In startPlanner(): " << mIter);

  if(mState==WAIT) {
    DBGA("startPlanner: State is WAIT!");
  }
  else {
    DBGA("startPlanner: State is Unknown!");
  }

  if(mState == INI) {
    mPlanner->startPlanner();
    setState(RUN);
  } else if(mState == WAIT) {

    DBGA("startPlanner: Starting planner");

    setCurrentHand(1);

    // Moving the second hand to position that doesn't influence the other one
    GraspPlanningState *mHandObjectState = new GraspPlanningState(mHand);
    Quaternion quat(1, 0, 0, 0);
    vec3 origin(1000, 1000, 1000);
    transf f(quat, origin);

    mHandObjectState->setRefTran(f);

    mPlanner->showGrasp(mHandObjectState);

    DBGA("startPlanner: Hand 2 moved");

    setCurrentHand(0);
    plannerInit_clicked();

    DBGA("startPlanner: Hand 1 reset");

    std::list<GraspPlanningState*>::iterator it = mBestListHand1.begin();
    std::advance(it, mIter);

    DBGA("startPlanner: Setting the gripper A to pose " << mIter);

    mPlanner->showGrasp(*it);

    mPlanner->debugRender();
    
    mHand->autoGrasp(true, 1.0);

    mIter++;

    // 
    setCurrentHand(1);
    plannerInit_clicked();

    DBGA("startPlanner: Starting planner again...");

    mPlanner->startPlanner();

    DBGA("startPlanner: Planner started...");
    setState(RUN);
  }

  DBGA("startPlanner: updating status");
	updateStatus();
  DBGA("startPlanner: status updated");
}

void EigenGraspPlannerDlg::stopPlanner()
{
	mPlanner->pausePlanner();
  setState(PAUSE);
	updateStatus();
}

void EigenGraspPlannerDlg::plannerStart_clicked()
{	
	if (!mPlanner->isActive() && (mState != FIN)){
		startPlanner();

    time(&start_timer);

    //startCompletePlanner();
	} else if(mPlanner->isActive()) {
		stopPlanner();
	}
}

void EigenGraspPlannerDlg::plannerTypeBox_activated( const QString & )
{
	if (mPlanner) {
		delete mPlanner;
		mPlanner = NULL;
	}
	updateStatus();
}

//----------------------------------- Dedicated on-line planner control ---------------------------

void EigenGraspPlannerDlg::autoGraspBox_clicked()
{

}

//this slot does the updating that's specific to the online planner
void EigenGraspPlannerDlg::onlinePlannerUpdate()
{
 assert( mPlanner->getType() == PLANNER_ONLINE);
 OnLinePlanner *op = (OnLinePlanner*)mPlanner;
 QString num;

 double objDist = op->getObjectDistance();
 num.setNum(objDist,'f',0);
 objDistLabel->setText("Object distance: "  + num);
/*
 double solDist = op->getSolutionDistance();
 if (solDist >= 0) num.setNum(solDist,'f',0);
 else num.setAscii("N/A");
 solDistLabel->setText("Solution distance: " + num);
*/
 ActionType s = op->getAction();
 switch (s) {
	 case ACTION_PLAN:
		 num.setAscii("PLANNING");
		 break;
	 case ACTION_GRASP:
		 num.setAscii("GRASPING");
		 break;
	 case ACTION_OPEN:
		 num.setAscii("OPEN");
		 break;
	 default:
		 num.setAscii("N/A");
		 break;
 }
 onlineStatusLabel->setText("Status: " + num);

 num.setNum( op->getSABufferSize() );
 saBufferLabel->setText("SimAnn buffer: " + num);
 num.setNum( op->getFCBufferSize() );
 fcBufferLabel->setText("FC Thread buffer: " + num); 
}

void EigenGraspPlannerDlg::onlineGraspButton_clicked()
{
 assert( mPlanner->getType() == PLANNER_ONLINE);
 OnLinePlanner *op = (OnLinePlanner*)mPlanner;
 op->action(ACTION_GRASP);
 onlinePlannerUpdate();
}

void EigenGraspPlannerDlg::onlineReleaseButton_clicked()
{
 assert( mPlanner->getType() == PLANNER_ONLINE);
 OnLinePlanner *op = (OnLinePlanner*)mPlanner;
 op->action(ACTION_OPEN);
 onlinePlannerUpdate();
}


void EigenGraspPlannerDlg::onlinePlanButton_clicked()
{
 assert( mPlanner->getType() == PLANNER_ONLINE);
 OnLinePlanner *op = (OnLinePlanner*)mPlanner;
 op->action(ACTION_PLAN);
 onlinePlannerUpdate(); 
}


void EigenGraspPlannerDlg::instantEnergyButton_clicked()
{
 assert(mPlanner);
// mPlanner->instantEnergy();
}


void EigenGraspPlannerDlg::showCloneBox_toggled( bool c)
{
 assert( mPlanner->getType() == PLANNER_ONLINE);
 OnLinePlanner *op = (OnLinePlanner*)mPlanner;
 op->showClone(c);  
}


void EigenGraspPlannerDlg::showSolutionBox_toggled( bool c)
{	
 assert( mPlanner->getType() == PLANNER_ONLINE);
 OnLinePlanner *op = (OnLinePlanner*)mPlanner;
 op->showSolutionClone(c);
}

void EigenGraspPlannerDlg::useVirtualHandBox_clicked()
{
	/*
 assert( mPlanner->getType() == PLANNER_ONLINE);
 OnLinePlanner *op = (OnLinePlanner*)mPlanner;
 bool c = useVirtualHandBox->isChecked();
 op->controlVirtualHand(c);
 */

}

void EigenGraspPlannerDlg::useRealBarrettBox_toggled( bool s)
{
 assert( mPlanner->getType() == PLANNER_ONLINE);
 OnLinePlanner *op = (OnLinePlanner*)mPlanner;
 op->useRealBarrettHand(s);
}

//---------------------------------------- Input selection -----------------------------------------

void EigenGraspPlannerDlg::inputGloveBox_toggled( bool on)
{
	assert(mPlanner);
	mPlanner->setInput(INPUT_GLOVE, on);
}

void EigenGraspPlannerDlg::inputLoadButton_clicked()
{
  time_t timer;
  time(&timer);

  SoOutput out;

  std::string filename = "~/grasps_results/" + objectName + std::to_string(timer) + ".iv";

  if(!out.openFile(filename.c_str()))
    DBGA("Error saving the .iv file");

  out.setBinary(false);
  SoWriteAction write(&out);

  write.apply( graspitCore->getIVmgr()->world->getIVRoot() );

  write.getOutput()->closeFile();
}