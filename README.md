GraspIt! Simulator - Grasp Planning Handoff Operations

#Running

1. Use catkin to build it
2. Execute it using rosrun

#Using it

1. Add one or two robots
2. Add at least one object
3. Open "EigenGrasp Planners"
4. Press "Init" and then ">"
5. The values of the first gripper position are written to "~/grasp_results/grasps.txt"

#Settings

1. The object position for the IK verification is on the lines 182 and 183 of the file "/src/EGPlanner/sinAnn.cpp"
2. The value of K is also hardcoded as a define "BEST_LIST_SIZE"

#Issues

1. Frequently crashes when in planner is set to run all the way through
2. There's a lot of stuff hardcoded

David Sousa