//######################################################################
//
// GraspIt!
// Copyright (C) 2002-2009  Columbia University in the City of New York.
// All rights reserved.
//
// GraspIt! is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GraspIt! is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with GraspIt!.  If not, see <http://www.gnu.org/licenses/>.
//
// Author(s): Matei T. Ciocarlie
//
// $Id: simAnn.cpp,v 1.30 2009/05/07 19:57:26 cmatei Exp $
//
//######################################################################

#include "EGPlanner/simAnn.h"

#include <time.h>

#include "EGPlanner/searchState.h"
#include "EGPlanner/energy/searchEnergy.h"

//#define GRASPITDBG
#include "debug.h"

#include <tf/tf.h>

#include <ctime>

#include "ikfastdemo.h"

#define TINY 1.0e-7

SimAnn::SimAnn(bool firstHand)
{
	isfirstHand = firstHand;

	setParameters(ANNEAL_DEFAULT);
	mWriteResults = false;
	mFile = NULL;

	if (mWriteResults) {
	} else {
		mFile = NULL;
	}
	mTotalSteps = 0;
}

SimAnn::SimAnn()
{
	setParameters(ANNEAL_DEFAULT);
	mWriteResults = false;
	mFile = NULL;

	if (mWriteResults) {
	} else {
		mFile = NULL;
	}
	mTotalSteps = 0;
}

SimAnn::~SimAnn()
{
	if (mWriteResults) fclose(mFile);
}

void
SimAnn::writeResults(bool w)
{
	if (w) {
		if (mWriteResults) {
			DBGA("Sim ann already writing");
			return;
		}
		mFile = fopen("simAnn.txt","a");
		mWriteResults = true;
	} else {
		if (mWriteResults) {
			fclose(mFile);
			mFile = NULL;
		} else {
			DBGA("Sim Ann was not writing");
		}
		mWriteResults = false;
	}
}

void SimAnn::setParameters(AnnealingType type)
{
	switch (type) {
		case ANNEAL_DEFAULT:
			//these are the default parameters that experimentation shows work best over 8 dimensions
			//identical schedule for error and neighbors
			//error is supposed to be distance between fingers and object, so raw order of 1-100 mm
			YC = 7.0;
			HC = 7.0;
			YDIMS = 8;
			HDIMS = 8;
			NBR_ADJ = 1.0;
			ERR_ADJ = 1.0e-6;
			DEF_T0 = 3.5e6;
			DEF_K0 = 20000;
			break;
		case ANNEAL_LOOP:
			//for looping we use a slightly shorter interval
			YC = 7.0;
			HC = 7.0;
			YDIMS = 8;
			HDIMS = 8;
			NBR_ADJ = 1.0;
			ERR_ADJ = 1.0e-6;
			DEF_T0 = 1.0e6;
			DEF_K0 = 35000;
			break;
		case ANNEAL_MODIFIED:
			//different version which doesn't really work better than default
			YC = 2.38;
			YDIMS = 8;
			HC = 2.38;
			HDIMS = 8;
			NBR_ADJ = 1.0e-3;
			ERR_ADJ = 1.0e-1;
			DEF_T0 = 1.0e3;
			DEF_K0 = 0;
			break;
		case ANNEAL_STRICT:
			DBGP("Strict Sim Ann parameters!");
			//this are for searches that ONLY look at grasp quality measure
			YC = 0.72; 
			HC = 0.22;
			YDIMS = 2;
			HDIMS = 2;
			NBR_ADJ = 1.0;
			ERR_ADJ = 1.0; //meant to work with ENERGY_STRICT_AUTOGRASP which multiplies eps. gq by 1.0e2
			DEF_T0 = 10.0;
			DEF_K0 = 0; 
			break;
		case ANNEAL_ONLINE:
			//parameters for on-line search. Supposed to work really fast and in fewer dimensions
			//YC = 0.54;
			YC = 0.24;
			HC = 0.54;
			//YDIMS = 3;
			YDIMS = 2;
			HDIMS = 3;
			NBR_ADJ = 1.0;
			ERR_ADJ = 1.0e-2;
			DEF_T0 = 1.0e1;
			DEF_K0 = 100;
			break;
		default:
			fprintf(stderr,"Unknown Annealing params requested, using default!\n");
			setParameters(ANNEAL_DEFAULT);
			break;
	}
}

void SimAnn::reset()
{
	srand( (unsigned)time(NULL) );
	mCurrentStep = DEF_K0;
	mT0 = DEF_T0;
}


/***

The object position is here!!!

***/
// Object position in MoveIt!
tf::Quaternion moveit_object_q(0.0, 0.0, 0.0, 1.0);
tf::Vector3    moveit_object_t(-0.35, 0.35, 0.125);
tf::Transform  moveit_object(moveit_object_q, moveit_object_t);      

// Transform from the GraspIt! gripper frame to the MoveIt! gripper frame
//tf::Quaternion graspit_gripper_to_object_q(0,0,0,0); //= EulerZYX_to_Quaternion(0.0, -M_PI/2, 0.0);
//tf::Vector3    graspit_gripper_to_object_t(0.0, 0.0, 0.0);
//tf::Transform  graspit_gripper_to_object(graspit_gripper_to_object_q, graspit_gripper_to_object_t);


// Transform from the GraspIt! gripper frame to the MoveIt! gripper frame
tf::Quaternion moveit_to_graspit_gripper_q(0.0, -0.707, 0.0, 0.707); //= EulerZYX_to_Quaternion(0.0, -M_PI/2, 0.0);
tf::Vector3    moveit_to_graspit_gripper_t(-0.15, 0.0, 0.0);
tf::Transform  moveit_to_graspit_gripper(moveit_to_graspit_gripper_q, moveit_to_graspit_gripper_t);

//tf::Transform  graspit_gripper = moveit_object * graspit_gripper_to_object * moveit_to_graspit_gripper;

int n_ilegals = 0;
bool super_jump = false;

/*! The caller passes it a HandObjectState and a calculator that can be used
	to compute the quality (or in annealing terms "energy") of a HandObjectState. 
	This function computes the next state in the annealing schedule.

	See SimAnn::Result declaration for possible return values.
*/
SimAnn::Result 
SimAnn::iterate(GraspPlanningState *currentState, SearchEnergy *energyCalculator, GraspPlanningState *targetState)
{
	/***

		Depending on the result we can either add it to the mAvoidList or
		decrease the jump probability?

	***/

//	clock_t begin = clock();

	//using different cooling constants for probs and neighbors
	double T = cooling(mT0, YC, mCurrentStep, YDIMS);

	//attempt to compute a neighbor of the current state
	GraspPlanningState* newState;
	double energy; bool legal = false;
	int attempts = 0; int maxAttempts = 10;
	
	DBGP("Ngbr gen loop");

	GraspPlanningState* prevState;

	while (!legal && attempts <= maxAttempts) {
		// get a state in the neighborhood
		newState = stateNeighbor(currentState, T * NBR_ADJ, targetState);

		//energyCalculator->printAvoidList();

		prevState = newState;
		//DBGA("Analyze state...");
		energyCalculator->analyzeState(legal, energy, newState);
		//DBGA("Analysis done.");
		if (!legal) {
			n_ilegals++;

			if(n_ilegals > 35) {
				super_jump = true;
				n_ilegals = 0;
			}

			delete newState;
		} else if(mCurrentStep % 500 == 0 && isfirstHand/*&& ( (newState)->distance(currentState) > 25)*/ ) {
			
			n_ilegals = 0;

			// If it's legal test the IK
			PositionState *mPosition;
			mPosition = PositionState::createInstance(SPACE_COMPLETE, newState->getHand());
			mPosition = newState->getPosition();

			transf HandPos = mPosition->getCoreTran();

			vec3 		HandTranlation = HandPos.translation();
			Quaternion 	HandRotation   = HandPos.rotation();

			tf::Quaternion graspit_gripper_to_object_q(HandRotation.x, HandRotation.y, HandRotation.z, HandRotation.w);
			tf::Vector3    graspit_gripper_to_object_t(HandTranlation.x() * 0.001, HandTranlation.y() * 0.001, HandTranlation.z() * 0.001);
			tf::Transform  graspit_gripper_to_object(graspit_gripper_to_object_q, graspit_gripper_to_object_t);

			tf::Transform  graspit_gripper = moveit_object * graspit_gripper_to_object * moveit_to_graspit_gripper;

			tf::Vector3    gripper_t = graspit_gripper.getOrigin();
	    	tf::Quaternion gripper_q = graspit_gripper.getRotation();

	    	DBGA("In MoveIt! frame:");
			DBGA("Tx: " << gripper_t.getX() << " Ty: " << gripper_t.getY() << " Tz: " << gripper_t.getZ());
			DBGA("Qw: " << gripper_q.w() << " Qx: " << gripper_q.x() << " Qy: " << gripper_q.y() << " Qz: " << gripper_q.z());


	    	int res = ikcompute(    gripper_t.getX(),   // tx
	                            	gripper_t.getY(),   // ty
	                            	gripper_t.getZ(),   // tz
	                            	gripper_q.w(),    	// qw
	                            	gripper_q.x(),    	// qx
	                            	gripper_q.y(),    	// qy
	                            	gripper_q.z()  ); 	// qz

	    	DBGA("Got " << res << " solutions for the IK.");

	    	if(res <= 0) {
	    		DBGA("No IK solution, deleting...");

	    		energyCalculator->addToAvoidList(newState);

	    		T += 1000;

	    		legal = false;
	    		delete newState;
	    	}

	    	T += 100;
    	}

		attempts++;
	}

	if (!legal) {
		T += 10000;

		DBGA("Failed to compute a legal neighbor");
		//we have failed to compute a legal neighbor. 
		//weather the SimAnn should advance a step and cool down the temperature even when it fails to compute a 
		//legal neighbor is debatable. Might be more interactive (especially for the online planner) if it does.
		//mCurrentStep += 1;
		return FAIL;
	}

	//we have a neighbor. Now we decide if we jump to it.
	DBGP("Legal neighbor computed; energy: " << energy )
	newState->setEnergy(energy);
	newState->setLegal(true);
	newState->setItNumber(mCurrentStep);

	//using different cooling constants for probs and neighbors
	T = cooling(mT0, HC, mCurrentStep, HDIMS);

	double P = prob(ERR_ADJ*currentState->getEnergy(), ERR_ADJ*newState->getEnergy(), T);
	double U = ((double)rand()) / RAND_MAX;
	Result r = KEEP;
	if ( P > U ) {
		DBGP("Jump performed");
		currentState->copyFrom(newState);
		r = JUMP;

		if(energy < lowestEnergy) lowestEnergy = energy;

	} else{
		DBGP("Jump rejected");
	}

	if(mCurrentStep % 1000 == 0) {
		FILE * pFile;

		if(mCurrentStep == 1000) {
			pFile = fopen("/home/david/catkin_ws/lowestEnergy.txt", "w");
		} else {
			pFile = fopen("/home/david/catkin_ws/lowestEnergy.txt", "a");
		}
		fprintf(pFile, "%ld:%lf\n", mCurrentStep, lowestEnergy);

		fclose(pFile);
	}

	mCurrentStep+=1; mTotalSteps+=1;
	DBGP("Main iteration done.")
	delete newState;

	if (mWriteResults && mCurrentStep % 2 == 0 ) {
		assert(mFile);
		fprintf(mFile,"%ld %d %f %f %f %f\n",mCurrentStep,mTotalSteps,T,currentState->getEnergy(),
										 currentState->readPosition()->readVariable("Tx"),
										 targetState->readPosition()->readVariable("Tx"));
		//currentState->writeToFile(mFile);
	}

//	clock_t end = clock();
//	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
//  	DBGA("\n\nIterate run time: " << elapsed_secs);

	return r;
}

/* ------------------------------ NEIGHBOR GENERATION FUNCTION -------------------------- */

GraspPlanningState* SimAnn::stateNeighbor(GraspPlanningState *s, double T, GraspPlanningState *t)
{
	GraspPlanningState *sn = new GraspPlanningState(s);

	double super_jump_mult = 1.0;

	if(super_jump) {
		super_jump = false;
		super_jump_mult = 1.5;
	}

	if (t) {
		variableNeighbor( sn->getPosition(), T*super_jump_mult, t->getPosition() );
		variableNeighbor( sn->getPosture(), T*2*super_jump_mult, t->getPosture() );
	} else {
		variableNeighbor( sn->getPosition(), T*super_jump_mult, NULL );
		variableNeighbor( sn->getPosture(), T*2*super_jump_mult, NULL );
	}

	return sn;
}

void
SimAnn::variableNeighbor(VariableSet *set, double T, VariableSet *target)
{
	SearchVariable *var;
	double v,tv=0,conf=0;

	// Foreach varible (e.g.: Tx, Ty, Tz)
	for (int i=0; i < set->getNumVariables(); i++) {

		// gets one of the variables (e.g.: Tx)
		var = set->getVariable(i);

		if ( var->isFixed() ) {
			DBGA("Variable is fixed!");
			continue;
		}

		v = var->mMaxVal + 1.0; //start off ilegal
		int loop = 0;
		
		// while the variable is out of its range
		while ( v > var->mMaxVal || v < var->mMinVal ) {
			loop++;
			
			if (!target || !target->getVariable(i)->isFixed()) {
				//we have no target value; use regular sim ann neighbor distribution
				v = var->getValue() + neighborDistribution(T) * var->mMaxJump;
				//DBGA("Target doesn't exist. var->mMaxJump: " << var->mMaxJump);

			} else {
				//we have a target value and a confidence level
				tv = target->getVariable(i)->getValue();
				
				//DBGA(target->getVariable(i)->getName().toStdString().c_str() << " input: " << tv);
				
				conf = target->getVariable(i)->getConfidence();
				assert( conf >= 0 && conf <= 1);
				
				//normalize desired change to -1..1 interval relative to the max jump
				double change = tv - var->getValue();
				
				if (change > var->mMaxJump) 		  change = var->mMaxJump;
				else if (change < -1 * var->mMaxJump) change = -1 * var->mMaxJump;
				
				change = change / var->mMaxJump;
				
				//call the appropriate neighbor generator
				//DBGA(var->getName().toStdString().c_str() << " value: " << var->getValue() << " Target: " << tv << " Change: " << change);
				
				v = var->getValue() + biasedNeighborDistribution(T, change, conf) * var->mMaxJump;
			}

			if (var->isCircular()) {
			  DBGP("Circular variable! " << var->getName().toStdString().c_str());
				if ( v > var->mMaxVal) v -= var->getRange();
				else if ( v < var->mMinVal) v += var->getRange();
			}

			if ( v > var->mMaxVal && v - var->mMaxVal < TINY)  v = var->mMaxVal;
			if ( v < var->mMinVal && v - var->mMinVal > -TINY) v = var->mMinVal;

			if (loop == 100) {
				//DBGA("value: " << var->getValue() << " Mj: " << var->mMaxJump);
				//DBGA("min val: " << var->mMinVal << " max val: " << var->mMaxVal);
				
				if (target->getVariable(i)->isFixed()) {
					//DBGA("Target: " << tv << "; Nbr: " << biasedNeighborDistribution(T,tv - var->getValue(),conf));
				}

				break;
			}
		}

		if (loop > 10) DBGA("Neighbor gen loops: " << loop);
		
		//DBGA("Var: " << var->getName().toLatin1().data() << " old: " << var->getValue() << " new: " << v << " maxJump: " << var->mMaxJump);

		var->setValue(v);
	}
}

/* ------------------- SCHEDULING FUNCTIONS -------------------------*/
double
SimAnn::cooling(double t0, double c, int k, int d)
{
	double t;
	// Cauchy cooling schedule
	// return t0 / (double)k;

	//Ingber cooling schedule
	t = t0 * exp ( -c * (double)pow((double)k, 1.0/d) );
	return t;
}

double
SimAnn::prob(double e_old, double e_new, double T)
{
	if (e_new < e_old) return 1.0;
	return pow( M_E , (e_old - e_new) / T);
}

double
SimAnn::neighborDistribution(double T)
{	
	double y;

	//start with uniform distribution
	double u = ((double)rand()) / RAND_MAX;
	double v = fabs( 2.0 * u - 1.0);

	//Ingber's prob. distribution
	y = T * ( pow( 1.0+1.0/T , v ) - 1 );
	if ( u<0.5) y = -1.0 * y;

//	DBGA("Jump: " << y);

	return y;
}

double 
SimAnn::neighborInverse(double T, double y)
{
	double u = log(1+fabs(y)/T)/log(1+1.0/T);
	if ( y<0) u = -1.0 * u;
	return u;
}

double
SimAnn::biasedNeighborDistribution(double T, double in, double conf)
{
	double u1,u2,u;
	double sigma_sq,mean;

	//we get the confidence as a linear value between 0 and 1

	//if we don't invert the nbr fct first, a variance of 0.1 seems to be "average confidence"
	//convert to a log so that we exploit mainly the area between 0 and 0.1
	//this also inverts it, mapping conf = 1 (max confidence) to sigma = 0 (always produce a 0)
	//if (conf < 0.01) sigma_sq = 1;
	//else sigma_sq = -log( conf ) / 10;	

	//for the case where we use the inverse neighbor, it's best to set the variance more conservatively
	//which means that most play is actually between 0.1 and 1
	//so all we gotta do is invert the confidence
	sigma_sq = 1.0 - conf;

	DBGP("In: " << in << " conf: " << conf << " simga_sq: " << sigma_sq);
	
	//the target value we get here is normalized and capped to -1..1
	//we set the mean of the gaussian to the value that the neighbor function would map to our target value
	mean = neighborInverse(T,in);
	//alternatively, we could just set it directly to the target and let the neighbor fctn map it to 
	//something depending on the temperature, like this:
	//mean = in;
	//but this means that even for infinitely strict target (confidence = 1.0), we can never reach it exactly
	//as the nbr fctn will map it to smaller and smaller steps.

	//compute a normal distribution, centered at input and clamped btw -1 and 1
	u = 2;
	double loops = 0;
	while ( u>1 || u<-1) {
		//start with uniform distribution
		u1 = ((double)rand()) / RAND_MAX;
		u2 = ((double)rand()) / RAND_MAX;

		//turn it into a normal distribution with the Box-Muller transform
		u = sqrt( -2 * log(u1) ) * cos(2*M_PI*u2);
		
		//set variance based on confidence and mean based on input
		u = mean + sqrt(sigma_sq) * u;
		loops++;
	}
	//just check that we're not spending too much time here
	if (loops > 20) DBGA("Biased distribution loops:  " << loops);

	//use it to generate Ingber neighbor
	double y = T * ( pow( 1.0+1.0/T , fabs(u) ) - 1 );
	if ( u < 0) y = -1.0 * y;
	DBGP("u: " << u << " y: " << y << " loops: " << loops);

	return y;
}
