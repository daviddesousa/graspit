
#include "EGPlanner/energy/contactEnergy.h"
#include "robot.h"
#include "grasp.h"
#include "debug.h"
#include "world.h"
#include "body.h"
#include "contact/virtualContact.h"
#include "searchStateImpl.h"

#include <stdlib.h>

#include <tf/tf.h>

#include "ikfastdemo.h"

std::vector<position> vertices;
Body *object;

/*

// Object position in MoveIt!
tf::Quaternion moveit_object_q(0.0, 0.0, 0.0, 1.0);
tf::Vector3    moveit_object_t(-0.35, 0.35, 0.125);
tf::Transform  moveit_object(moveit_object_q, moveit_object_t);      

// Transform from the GraspIt! gripper frame to the MoveIt! gripper frame
tf::Quaternion graspit_gripper_to_object_q(0,0,0,0); //= EulerZYX_to_Quaternion(0.0, -M_PI/2, 0.0);
tf::Vector3    graspit_gripper_to_object_t(0.0, 0.0, 0.0);
tf::Transform  graspit_gripper_to_object(graspit_gripper_to_object_q, graspit_gripper_to_object_t);


// Transform from the GraspIt! gripper frame to the MoveIt! gripper frame
tf::Quaternion moveit_to_graspit_gripper_q(0.0, -0.707, 0.0, 0.707); //= EulerZYX_to_Quaternion(0.0, -M_PI/2, 0.0);
tf::Vector3    moveit_to_graspit_gripper_t(-0.15, 0.0, 0.0);
tf::Transform  moveit_to_graspit_gripper(moveit_to_graspit_gripper_q, moveit_to_graspit_gripper_t);

tf::Transform  graspit_gripper = moveit_object * graspit_gripper_to_object * moveit_to_graspit_gripper;

*/

double ContactEnergy::energy() const
{
    /*
    this is if we don't want to use pre-specified contacts, but rather the closest points between each link
    and the object all iterations. Not necessarily needed for contact energy, but useful for GQ energy
    */
    if (mContactType == CONTACT_LIVE && mType != ENERGY_AUTOGRASP_QUALITY && mType != ENERGY_STRICT_AUTOGRASP)
    {
        mHand->getWorld()->findVirtualContacts(mHand, mObject);
        DBGP("Live contacts computation");
    }

    mHand->getGrasp()->collectVirtualContacts();

    //DBGP("Contact energy computation")
    //average error per contact
    VirtualContact *contact;
    vec3 p,n,cn;
    double totalError = 0;

    double totalDistance = 0;
    double totalAngle = 0;
    double totalVerticeDist = 0;

    // If the second hand is defined let's use it as a parameter to evaluate the quality of the grasp
    // Maximise the angle to close of 180º (or to the desired angle between the grippers)
    if(mOtherHand != NULL) {

        //DBGA("Using the angle between the grippers");

        mOtherHand->getGrasp()->collectVirtualContacts();

        transf mHandTran = (mHand->getTran());
        transf mOtherHandTran = (mOtherHand->getTran());

        vec3 axis_start(-1.0, 0.0, 0.0);

        vec3 mHandAxis      = mHandTran.rotation() * axis_start;
        vec3 mOtherHandAxis = mOtherHandTran.rotation() * axis_start;

        double angle_between_axis = (mOtherHandAxis % mHandAxis ) / (mHandAxis.len() * mOtherHandAxis.len());

        double tmp_ang = acosf(angle_between_axis) * 57.2957795;

        //DBGA("Angle between hands: " << tmp_ang << " error: " << tmp_ang * 0.05);

        if(tmp_ang > 120)
            totalError += -tmp_ang * 0.25;
        else
            totalError += (tmp_ang*tmp_ang) * 0.075;
    }

    //! Get the object vertices
    if(vertices.size() == 0 || mObject != object) {

        object = mObject;

        vertices.clear();

        mObject->getGeometryVertices(&vertices);

        // The vector returned by the previous function has duplicates so we have
        // to remove them
        for(int i=0; i < vertices.size(); i++) {
            for(int j=0; j < vertices.size(); j++) {

                if(i != j) {
                    if( vertices[i].x() == vertices[j].x() &&
                        vertices[i].y() == vertices[j].y() &&
                        vertices[i].z() == vertices[j].z() ) {

                        vertices.erase( vertices.begin() + j );
                        j = 0;
                    }
                }

            }
        }

        // Set the vertices ti the world referencial
        for(int i=0; i < vertices.size(); i++) {
            vertices[i] = vertices[i] * mObject->getTran();
        }

        // More debug...
        for (int i=0; i < mHand->getGrasp()->getNumContacts(); i++)
        {
            contact = (VirtualContact*)mHand->getGrasp()->getContact(i);
            position cp = contact->getWorldLocation();

            vec3 vector(cp.x() - vertices[i].x(), cp.y() - vertices[i].y(), cp.z() - vertices[i].z());

//            DBGA("Contact point(" << i << "): " << cp << " - " << vector.len());
        }
    }

    transf objTran = mObject->getTran();

    vec3 objTranslation = objTran.translation();

    std::vector<double> distance_contactpoints;

    double distance[mHand->getGrasp()->getNumContacts()], angle[mHand->getGrasp()->getNumContacts()];
    vec3 normals[mHand->getGrasp()->getNumContacts()];

    //! Distance and angle of contact points
    /**
            Finger index:
                0 - Index
                1 - Thumb
                2 - Pinkie
    */
    for (int i=0; i < mHand->getGrasp()->getNumContacts(); i++)
    {
        contact = (VirtualContact*)mHand->getGrasp()->getContact(i);

        contact->getObjectDistanceAndNormal(mObject, &p, NULL);
        double dist = p.len();

        //this should never happen anymore since we're never inside the object
        //if ( (-1.0 * p) % n < 0) dist = -dist;

        //BEST WORKING VERSION, strangely enough
        double abs_dist = fabs(dist);
        totalError += abs_dist * 1.5;
        distance[i] = fabs(dist);
        //DBGA("Distance: " << abs_dist);
        //DBGA("Distance(" << i << "): " << abs_dist);
        distance_contactpoints.push_back(abs_dist);        

        //totalDistance += abs_dist;

        //let's try this some more
        //totalError += distanceFunction(dist);
        //cn = -1.0 * contact->getWorldNormal();

        //new version
        cn = contact->getWorldNormal();     // Get normal vector of the contact?
        n = normalise(p);                   // Unit vector
        normals[i] = n;
        double d = 1 - cn % n;              // % == dot product
        double dw = d * 5.0;
        totalError += dw;      // * 50 ??
        //DBGA("Angular diff: " << dw);
        totalAngle += dw;

        /*** Distance of the contact points to the center of the object***/
        position contactPos = contact->getWorldLocation();        
        vec3 diff(objTranslation.x() - contactPos.x(), objTranslation.y() - contactPos.y(), objTranslation.z() - contactPos.z());

        //DBGA("Distance from CoM: " << diff.len() * 0.01);
        //totalError -= diff.len() * 0.01;


        position cp = contact->getWorldLocation();

        double vd = 0;
        std::vector<double> distances;

        for(int i=0; i<vertices.size(); i++) {
            vec3 vector(cp.x() - vertices[i].x(), cp.y() - vertices[i].y(), cp.z() - vertices[i].z());
            //DBGA("vet: " << vector << " len:" << vector.len());
            
            distances.push_back(vector.len());
        }

        sort(distances.begin(), distances.end());

        for(int i=0; i < vertices.size() / 4; i++)
            vd += distances[i] * 0.05;

        vd /= (vertices.size() / 6);

        totalVerticeDist += vd;
//        DBGA("Vertice dist: " << vd);
        totalError += vd;

/*        std::vector<BoundingBox> bb;
        contact->getBoundingVolume(mObject, 200, &bb);
        for(int i=0; i < bb.size(); i++)
            DBGA("boundingBox(" << i << "): " << bb[i].halfSize);
*/
    }

    //! Angle between the normals of the surface of closest contact points
    //DBGA(normals[0].x() << ":" << normals[0].y() << ":" << normals[0].z());
    //DBGA(normals[1].x() << ":" << normals[1].y() << ":" << normals[1].z());
    //DBGA(normals[2].x() << ":" << normals[2].y() << ":" << normals[2].z());

    //DBGA("Len TI: " << normals[0].len() << " and " << normals[1].len() << " or " << normals[2].len());
    //DBGA("Cos TI: " << (normals[1] % normals[0]) / (normals[1].len() * normals[0].len()));
    //DBGA("Rad TI: " << acosf((normals[1] % normals[0]) / (normals[1].len() * normals[0].len())));

    double thumb_index_ang  = acosf((normals[1] % normals[0]) / (normals[1].len() * normals[0].len())) * 57.2958; //(1 - normals[1] % normals[0]) * 57.2958;
    double thumb_pinkie_ang = acosf((normals[1] % normals[2]) / (normals[1].len() * normals[2].len())) * 57.2958; //(1 - normals[1] % normals[2]) * 57.2958;
    double pinkie_index_ang = acosf((normals[2] % normals[0]) / (normals[2].len() * normals[0].len())) * 57.2958; //(1 - normals[2] % normals[0]) * 57.2958;

    //DBGA("Angle TI: " << thumb_index_ang);
    //DBGA("Angle TP: " << thumb_pinkie_ang);
    //DBGA("Angle PI: " << pinkie_index_ang);

    if(thumb_index_ang > 95 && thumb_pinkie_ang > 95) {
        // all good
    } else if(thumb_index_ang > 95 || thumb_pinkie_ang > 95) {
        totalError += 5;
    } else {
        totalError += 25;
    }

    //DBGA("Total angle: " << totalAngle);

    double tempTotalDist = 0;

    for(int i=0; i < distance_contactpoints.size(); i++) {
        tempTotalDist += distance_contactpoints[i];
    }

    //DBGA("Total distance: " << tempTotalDist);

    //! Give the largest contact points distance a larger penalization so they won't average out
    sort(distance_contactpoints.begin(), distance_contactpoints.end());
    reverse(distance_contactpoints.begin(), distance_contactpoints.end());

    double totalDist=0;

    for(int i=0; i < distance_contactpoints.size(); i++) {
        totalDist += distance_contactpoints[i] * 2/(i+1) * 1.15;
    }

    //DBGA("Total distance v2: " << totalDist);
    //DBGA("Total vertice dist: " << totalVerticeDist);
    //DBGA("Total: " << totalError);

    //DBGA("Total dist: " << totalDist);
    totalError += totalDist;

    if(mOtherHand != NULL) {

        //DBGA("Using the distance between the grippers");

        double handDist = 0;

        for (int j=0; j < mOtherHand->getGrasp()->getNumContacts() && j < mHand->getGrasp()->getNumContacts(); j++)
        {
            VirtualContact* contact1 = (VirtualContact*)mOtherHand->getGrasp()->getContact(j);
            VirtualContact* contact2 = (VirtualContact*)mHand->getGrasp()->getContact(j);

            vec3 cont_dist = (contact1->getWorldLocation() - contact2->getWorldLocation());

            handDist -= cont_dist.len();
        }

        //DBGA("Distance between hand contact points: " << handDist << " and error: " << handDist * 0.015);

        totalError += handDist * 0.015;
    }

    totalError /= mHand->getGrasp()->getNumContacts();
//    DBGA("Distance\tAngle\tVertices\n" << totalDistance << "\t" << totalAngle << "\t" << totalVerticeDist);
//    DBGA("Energy: " << totalError);

    //DBGP("Contact energy: " << totalError);
    return totalError;
}
