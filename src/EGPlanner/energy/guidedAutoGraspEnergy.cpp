#include "EGPlanner/energy/guidedAutoGraspEnergy.h"
#include "robot.h"
#include "grasp.h"
#include "debug.h"
#include "world.h"
#include "quality.h"
#include "contact/virtualContact.h"


/*!	This formulation combines virtual contact energy with autograsp energy. Virtual contact energy is used to "guide"
    initial	stages of the search and to see if we should even bother computing autograsp quality. Autograsp is a couple
    of orders of magnitude higher and so should work very well with later stages of the sim ann search
*/
double
GuidedAutoGraspQualityEnergy::energy() const
{
    //first compute regular contact energy; also count how many links are "close" to the object
        VirtualContact *contact;
        vec3 p,n,cn;
        double virtualError = 0; int closeContacts=0;

        double distance[mHand->getGrasp()->getNumContacts()], angle[mHand->getGrasp()->getNumContacts()];
        vec3 normals[mHand->getGrasp()->getNumContacts()];

        //collect virtual contacts first
        mHand->getGrasp()->collectVirtualContacts();
        /**
            Finger index:
                0 - Index
                1 - Thumb
                2 - Pinkie
        */
        for (int i=0; i<mHand->getGrasp()->getNumContacts(); i++) {

            contact = (VirtualContact*)mHand->getGrasp()->getContact(i);
            contact->getObjectDistanceAndNormal(mObject, &p, &n);

            double dist = p.len();
            if ( (-1.0 * p) % n  < 0) dist = -dist;

            //BEST WORKING VERSION, strangely enough
            virtualError += fabs(dist);
            distance[i] = fabs(dist);
            DBGA("Distance(" << i << "): " << dist);
            
            cn = -1.0 * contact->getWorldNormal();
            normals[i] = n;
            double d = 1 - cn % n;
            angle[i] = d;
            DBGA("The angle is: " << d * 57.2958);
            virtualError += d * 100.0 / 2.0;

            //if ( fabs(dist)<20 && d < 0.35 ) closeContacts++;
        }

        //! If the angle of the 
        double thumb_index_ang  = acosf((normals[1] % normals[0]) / (normals[1].len() * normals[0].len())) * 57.2958;//(1 - normals[1] % normals[0]) * 57.2958;
        double thumb_pinkie_ang = acosf((normals[1] % normals[2]) / (normals[1].len() * normals[2].len())) * 57.2958;//(1 - normals[1] % normals[2]) * 57.2958;
        double pinkie_index_ang = acosf((normals[2] % normals[0]) / (normals[2].len() * normals[0].len())) * 57.2958;//(1 - normals[2] % normals[0]) * 57.2958;

        DBGA("Angle TI: " << thumb_index_ang);
        DBGA("Angle TP: " << thumb_pinkie_ang);
        DBGA("Angle PI: " << pinkie_index_ang);

        virtualError /= mHand->getGrasp()->getNumContacts();

        //! If the the finger and the thumb are opposing and near a surface
        if(distance[0]<20 & distance[1]<20 & angle[0]<0.4 & angle[1]<0.4) closeContacts = 2;

        //if more than 2 links are "close" go ahead and compute the true quality
        double volQuality = 0, epsQuality = 0;
        if (closeContacts >= 2) {
            mHand->autoGrasp(false, 1.0);
            //now collect the true contacts;
            mHand->getGrasp()->collectContacts();
            if (mHand->getGrasp()->getNumContacts() >= 4) {
                mHand->getGrasp()->updateWrenchSpaces();
                volQuality = mVolQual->evaluate();
                epsQuality = mEpsQual->evaluate();
                if (epsQuality <= -1) epsQuality = 0; //QM returns -1 for non-FC grasps
            }

            DBGA("Virtual error " << virtualError << " and " << closeContacts << " close contacts.");
            DBGA("Volume quality: " << volQuality << " Epsilon quality: " << epsQuality);
        }
        else {
            virtualError *= 1.5;
        }

        //now add the two such that the true quality is a couple of orders of magn. bigger than virtual quality
        double q;
        if ( volQuality == 0) q = virtualError;
        else q = virtualError - volQuality * 1.0e3;
        if (volQuality || epsQuality) {DBGA("Final quality: " << q);}

        //DBGP("Final value: " << q << std::endl);
        return q;
}

 